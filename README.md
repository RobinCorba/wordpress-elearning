# Website of the WordPress E-Learning

* WordPress-based website developed by [Robin Corba](https://www.robincorba.com)

___
## Update instructions

Please note that *all* package versions are locked to improve handovers to new developers. To update this project you have to manually adjust versions in the following files:

```
./Dockerfile
./docker-compose.yml
/src/composer.json
/src/app/web/themes/composer.json
/src/app/web/themes/package.json
```
___
## Installation instructions

### Prerequisites
Make sure you have this software installed on your computer:

| Software        | Version       |
| -------------   | ------------- |
| `docker`        | 20.10         |

___
### How to set up the development environment
1. Copy the `.env.example` to `.env`
1. Run `docker compose up`
1. Access the website on http://localhost. Happy coding! (next time you only have to run `docker compose up -d`)

___
### Notes
1. Access CMS on http://localhost/wp-admin

___
## Testing
This web application runs on the following software versions:
| Software        | Version       |
| -------------   | ------------- |
| `php`           | 7.4           |
| `mariadb`       | 10.5          |
| `WordPress`     | 6.0           |
| `Node`          | 15            |
| `NPM`           | 7.5           |
| `Composer`      | 2.0           |
| `Yarn`          | 1.22          |
| `Webpack`       | 3.10          |
