# Contributing to this repository

* [Getting started](#getting-started)
  + [Don't see your issue? Open one](#dont-see-your-issue-open-one)
  + [Ready to make a change? Create a new branch](#ready-to-make-a-change-create-a-new-branch)
  + [Make your update](#make-your-update)
  + [Open a Merge Request](#open-a-merge-request)
  + [Submit your MR & get it reviewed](#submit-your-mr--get-it-reviewed)
  + [Your MR is reviewed!](#your-mr-is-reviewed)
* [Reviewing](#reviewing)
  + [Self review](#self-review)
* [Deploying](#deploying)

___
## Getting started
Before you begin:
- This project runs locally in Docker containers, refer to [README.md](README.md) to see if you have the correct version.


### Don't see your issue? Open one
If you spot something new, open an issue in GitLab.
We'll use the issue (key) to communicate with the client and track our branches in our version control.


### Ready to make a change? Create a new branch
- We generally create a new branch from the `develop` branch for each change
- The name of a feature branch follows the following format `feature-<JIRA issue key>-name-of-the-feature`
- The name of a bug branch follows the following format `fix-<JIRA issue key>-name-of-the-bug-fix`
- The name of an update branch follow the following format `maintenance-update-name` (e.g. `maintenance-update-q2-2022`)


### Make your update
Make your changes to the file(s) you'd like to update.
Make sure you also update the `/CHANGELOG.md` and version number in `src/web/app/themes/wordpress-elearning/package.json` as well.


### Open a Merge Request
When you're done making changes and you'd like to propose them for review, create a new Merge Request in Gitlab to branch `develop`.
Make sure your MR contains the following elements:

- Title: `[JIRA issue key] Description of the change`
- Description: add a list of changes, for example:

```
This update will;

1. Update events plugin to v1.2.0
1. Update the e-books plugin to v.1.3.1
1. Show the "open form button" field on the homepage sections
```

Set the Assignee to yourself and the Reviewer to a colleague of which you have asked for their availability, of course. <3


### Submit your MR & get it reviewed
- Once you submit your MR, your colleague will review it with you. The first thing you're going to want to do is a [self review](#self-review).
- After that, your colleague may have questions, check back on your MR to keep up with the thread.

### Your MR is reviewed!
- Check for any threads (comments) placed by your colleague and provide additional information or fixes based on that input.
- Instead of adding "fix commits" based on MR feedback, try to fix the original commit, use the `git commit --fixup <hash>` flag together with a `git rebase <hash>` where your branch started.
- Resolve all threads before merging it into the `develop` branch


___
## Reviewing
The purpose of reviews is to create the best quality we can for our clients. Please keep in mind that,

- Reviews are always respectful, acknowledging that everyone did the best possible job with the knowledge they had at the time.
- Reviews discuss content, not the person who created it.
- Reviews are constructive and start conversation around feedback.


### Self review
You should always review your own MR first.

- [ ] Confirm that your changes are in line with the expectations of the client by reviewing the JIRA issue.
- [ ] Confirm that your change is compatible with browsers and screen sizes according to the Definition of Done (consult the Project Lead if you don't have this list).
- [ ] Compare your MR and check if all intended changes are there, including checking for any typo's.
- [ ] Review the content for technical accuracy.
- [ ] Copy-edit the changes for grammar and spelling.
- [ ] If there are any pipelines that fail in your MR, troubleshoot them until they're all passing.


___
## Deploying
This clients uses two environments; an acceptance and a production server.
Our GitLab deployment pipeline is automatically triggered to update these environments:

- Deploy to the acceptance server: commit/merge to the `develop` branch
- Deploy to production: add a (version) tag to the `main` branch

### Add a version tag
You can add a tag to `main` branch using the Gitlab.com GUI or with this command (example): `git tag v1.2.1`

Note that we use [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and the tag has to match the version mentioned in `/CHANGELOG.md` and `src/web/app/themes/erasmustrainingcentre/package.json`
